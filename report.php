<?php
// copy justified APhe 2012
?>

<?php
//session_start();
//define('RESTRICTED', TRUE);
require_once 'includes/includes.php';
if (!$blur->login_session) {
    header("Location: index.php");
    session_destroy();
}
include 'header.php';
?>

<div id="TabbedPanels1" class="TabbedPanels">
    <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0" id="login">Terminal Login</li>
        <li class="TabbedPanelsTab" tabindex="0" id="monitoring">Monitoring</li>
        <li class="TabbedPanelsTab" tabindex="0" id="change">Change Terminal PIN</li>
        <li class="TabbedPanelsTab" tabindex="0" id="download">Download Data</li>
        <li class="TabbedPanelsTab" tabindex="1" id="info">Invoice</li>
        <li class="TabbedPanelsTab" tabindex="0" id="inquiry">Inquiry</li>
        <li class="TabbedPanelsTab" tabindex="0" id="pembayaran">Payment</li>
        <li class="TabbedPanelsTab" tabindex="0" id="pelunasan">Acquittance</li>
        <li class="TabbedPanelsTab" tabindex="0" id="settlement">Settlement</li>
    </ul>
    <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" id="login_page"></div>
        <div class="TabbedPanelsContent" id="monitoring_page"></div>
        <div class="TabbedPanelsContent" id="change_page"></div>
        <div class="TabbedPanelsContent" id="download_page"></div>
        <div class="TabbedPanelsContent" id="info_page"></div>
        <div class="TabbedPanelsContent" id="inquiry_page"></div>
        <div class="TabbedPanelsContent" id="pembayaran_page"></div>
        <div class="TabbedPanelsContent" id="pelunasan_page"></div>
        <div class="TabbedPanelsContent" id="settlement_page"></div>
    </div>
</div>
<script type="text/javascript">
    var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
<script type="text/javascript">
$("#login").ready(function(){$.blockUI({message:'<img src = "css/images/loading2.gif" width="auto">',centerX:!0,centerY:!0});$("#login_page").hide("fast");$("#login_page").load("tab.php?method=login",function(){$.unblockUI();$("#login_page").show("slow")})});
</script>
<?php
include 'footer.php';
?>