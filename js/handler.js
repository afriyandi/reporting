$(document).ready(function fusion() {
    $("#logout").click(function() {
        $.get('out.php');
        alert('You have been loged out');
        location.reload();
        return false;
    })
    $("#login").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#login_page").hide("slow");
        $("#login_page").load('tab.php?method=login', function() {
            $.unblockUI();
            $("#login_page").show("slow");
        })
    })
    $("#monitoring").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#monitoring_page").hide("slow");
        $("#monitoring_page").load('tab.php?method=monitoring', function() {
            $.unblockUI();
            $("#monitoring_page").show("slow");
        })
    })
    $("#change").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#change_page").hide("slow");
        $("#change_page").load('tab.php?method=change', function() {
            $.unblockUI();
            $("#change_page").show("slow");
        })
    })
    $("#download").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#download_page").hide("slow");
        $("#download_page").load('tab.php?method=download', function() {
            $.unblockUI();
            $("#download_page").show("slow");
        })
    })
    $("#info").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#info_page").hide("slow");
        $("#info_page").load('tab.php?method=info', function() {
            $.unblockUI();
            $("#info_page").show("slow");
        })
    })
    $("#inquiry").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#inquiry_page").hide("slow");
        $("#inquiry_page").load('tab.php?method=inquiry', function() {
            $.unblockUI();
            $("#inquiry_page").show("slow");
        })
    })
    $("#pembayaran").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#pembayaran_page").hide("slow");
        $("#pembayaran_page").load('tab.php?method=pembayaran', function() {
            $.unblockUI();
            $("#pembayaran_page").show("slow");
        })
    })
    $("#pelunasan").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#pelunasan_page").hide("slow");
        $("#pelunasan_page").load('tab.php?method=pelunasan', function() {
            $.unblockUI();
            $("#pelunasan_page").show("slow");
        })
    })
    $("#settlement").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#settlement_page").hide("slow");
        $("#settlement_page").load('tab.php?method=settlement', function() {
            $.unblockUI();
            $("#settlement_page").show("slow");
        })
    })
    $("#sub", "#go").button().removeClass('active');
    $("#stid").click(function() {
        var clicked = parseInt($(this).val()) + 1;
        clicked = clicked + clicked;
        if (!$(this).hasClass('active')) {
            $.blockUI({
                message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
                centerX: true,
                centerY: true
            });
            $("#sstid").hide("slow");
            $("#sstid").load('search.php?method=tid', function() {
                $.unblockUI();
                $("#sstid").show("slow");
            });
            var clicked = 0;
        } else {
            if (click == '3') {
                $(this).removeClass('active');
            }
            return false;
        }
        $(this).addClass('active');
    })
    $("#sdate").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#ssdate").load('search.php?method=date', function() {
            $.unblockUI();
            $("#ssdate").show("slow");
        })
    })
    $("#change_pass").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#cpage").hide("slow");
        $("#cpage").load('umanage.php?method=change', function() {
            $.unblockUI();
            $("#cpage").show("slow");
        })
    })
    $("#c_user").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#crepage").hide("slow");
        $("#crepage").load('umanage.php?method=create', function() {
            $.unblockUI();
            $("#crepage").show("slow");
        })
    })
    $("#del_user").click(function() {
        $.blockUI({
            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
            centerX: true,
            centerY: true
        });
        $("#dpage").hide("slow");
        $("#dpage").load('umanage.php?method=del', function() {
            $.unblockUI();
            $("#dpage").show("slow");
        })
    })

})
