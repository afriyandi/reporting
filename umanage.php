<?php
// copy justified APhe 2012
?>

<?php
include 'includes/includes.php';
$logged = $blur->loged();

if ($logged == false) {
    header("Location: index.php");
    session_destroy();
}


$method = $_GET["method"];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    switch ($method) {
        case "change":
            if (empty($_POST["new1"]) || empty($_POST["new2"]) || empty($_POST["lpass"]))
                echo "At least put in something to the box above", exit();
            $blur->check_uname();
            if ($blur->check_password($blur->user, md5($_POST["lpass"]), $blur->comp)) {
                if ($blur->check_newpass($_POST["new1"], $_POST["new2"])) {
                    $go = TRUE;
                    $blur->changePass($blur->password);
                } else {
                    echo "Passwords are mismatch AND OR less than " . Param::getDabase()["PASS_LONG"] . " characters AND OR contain spaces AND OR maybe you change nothing?";
                    exit();
                }
            } else {
                echo "Wrong Password";
                exit();
            }

            break;
        case "create":
            if (empty($_POST["new1"]) || empty($_POST["new2"]) || empty($_POST["uname"]))
                echo "At least put in something to the box above", exit();

            if (!$blur->check_newpass($_POST["new1"], $_POST["new2"])) {
                echo "Passwords are mismatch AND OR less than " . Param::getDabase()["PASS_LONG"] . " characters AND OR contain spaces";
                exit();
            }

            if (!$blur->check_available($_POST["uname"])) {
                echo "This User name already exist";
                exit();
            }

            $blur->create_user($_POST["uname"], $blur->password);
            $go = TRUE;

            break;
        case "del" :
            if (empty($_POST["uname"]))
                echo "no username", exit();
            $blur->get_user();
            if(!$blur->check_user($_POST["uname"]))
                echo "user with username ".$_POST["uname"]." not exist", exit();
            $blur->delete_user($_POST["uname"]);
            $go = TRUE;
            break;
        default :
            break;
    }

    if ($go) {
        switch ($method) {
            case "change":
                echo "password successfully changed";
                break;
            case "create":
                echo "User created successfully";
                break;
            case "del":
                echo "Delete user ".$_POST["uname"]." complete";
            default :
                break;
        }
    }
} else {
    switch ($method) {
        case "change":
            $blur->check_uname();
            ?>
            <script>
                $(document).ready(function() {
                    $("#go").click(function() {
                        $.blockUI({
                            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
                            centerX: true,
                            centerY: true
                        });
                        $("#disp").hide("slow");
                        $('#disp').load('umanage.php?method=change', {
                            lpass: $('#last_pass').val(),
                            new1: $('#newpass1').val(),
                            new2: $('#newpass2').val()
                        }, function() {
                            $.unblockUI();
                            $('#disp').show("slow")
                            $('#disp').delay(5000).hide("blind")
                        })
                    });

                });
            </script>
            <div>
                Username : <?php echo $blur->user; ?>
            </div>
            <div>
                Previous Password : <input type="password" id="last_pass" title="type previous password">
            </div>
            <div>
                New Password : <input type="password" id="newpass1" title="type new password">
            </div>
            <div>
                New Password : <input type="password" id="newpass2" title="type new password again">
            </div>
            <div>
                <button id="go" type="submit">Change Password</button>
            </div>
            <div id="disp"><?php if (isset($go)) echo $go ?></div>

            <?php
            break;
        case "create":
            ?>
            <script>
                $(document).ready(function() {
                    $("#create").click(function() {
                        $.blockUI({
                            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
                            centerX: true,
                            centerY: true
                        });
                        $("#disp_cr").hide("slow");
                        $('#disp_cr').load('umanage.php?method=create', {
                            uname: $('#uname').val(),
                            new1: $('#newpass1').val(),
                            new2: $('#newpass2').val()
                        }, function() {
                            $.unblockUI();
                            $('#disp_cr').show("slow")
                            $('#disp_cr').delay(5000).hide("blind")
                        })
                    });

                });
            </script>
            <div>
                Username : <input type="text" id="uname" title="type new password">
            </div>
            <div>
                Password : <input type="password" id="newpass1" title="type password">
            </div>
            <div>
                Password : <input type="password" id="newpass2" title="type password again">
            </div>
            <div>
                <button id="create" type="submit">Create User</button>
            </div>
            <div id="disp_cr"><?php if (isset($go)) echo $go ?></div>
            <?php
            break;
        case "del":
            $blur->get_user();
            ?>
            <script>
                $(function() {
                    var populate = [<?php
            $count = count($blur->user);
            $i = 0;
            foreach ($blur->user as $index) {
                $i++;
                $populate = $index["user"];
                echo "\"" . $populate . "\"";
                if ($i != $count)
                    echo ",";
            }
            ?>];
                    $("#uname").autocomplete({
                        source: populate
                    });

                });
                $(document).ready(function() {
                    $("#delete").click(function() {
                        $.blockUI({
                            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
                            centerX: true,
                            centerY: true
                        });
                        $("#disp_del").hide("slow");
                        $('#disp_del').load('umanage.php?method=del', {
                            uname: $('#uname').val()
                        }, function() {
                            $.unblockUI();
                            $('#disp_del').show("slow")
                            $('#disp_del').delay(5000).hide("blind")
                        })
                    });

                });
            </script>

            <style>
                .ui-autocomplete{position:absolute;top:0;left:0;cursor:default}* html .ui-autocomplete{width:1px}
            </style>
            <div>
                Username : <input type="text" id="uname" title="type user name, it's auto complete">
            </div>
            <div>
                <button id="delete" type="submit">Delete User</button>
            </div>
            <div id="disp_del"><?php if (isset($go)) echo $go  ?></div>

            <?php
            break;
        default;
            break;
    }
}
?>