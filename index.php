<?php
require "/includes/includes.php";
$legend = "please login";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $user = addslashes($_POST["user"]);
    $pwd = addslashes($_POST["pwd"]);
    $comp = $_POST["company"];
    $login = $blur->login($user, $pwd, $comp);

    if ($blur->login_session == false) {
        $legend = "Bad Login, Try Again";
    } else {
        $get->routine();
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=us-ascii">

        <title>Report Status</title>
        <link rel="stylesheet" href="css/css.css">
        <link href="css/redmond/jquery-ui-1.9.2.custom.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.js"></script>
        <script type="text/javascript" src="js/validator.js"></script>

    </head>
    <?php
    if (!$blur->login_session) {
        ?>

        <body>
            <form id="login" name="login" method="post" action="">
                <fieldset>
                    <legend><?php echo $legend ?></legend>
                    username : <br />
                    <label for="user"></label>
                    <input type="text" name="user" id="user" autocomplete="off"/>
                    <br />
                    password : <br />
                    <label for="pwd"></label>
                    <input type="password" name="pwd" id="pwd" />
                    <br />
                    Company : <select name="company">
                        <option value="2">MCF</option>
                        <option value="1">MAF</option>
                        <option value="3">MF</option>
                    </select>
                    <br/>
                    Remember : <input type="checkbox" name="remember" id="cuk" value="1" />
                    <input type="submit" name="sub" id="sub" value="login" />
                    <br />
                </fieldset>
            </form>
            <script type ="text/javascript">
                $("#sub").button();
                var frmvalidator = new Validator("login");
                frmvalidator.addValidation("user", "req", "Please enter Username");
                frmvalidator.addValidation("user", "regexp=^[A-Za-z]+$", "Only character allowed");
                frmvalidator.addValidation("pwd", "req", "Please enter Password");
            </script>
            <?php
        } else {
            header("Location: report.php");
        }
        ?>
    </body>
</html>
