<?php
/* kambing soon */
/* CAT:Pie charts */

/* pChart library inclusions */
include("/pData.class.php");
include("/pDraw.class.php");
include("/pPie.class.php");
include("/pImage.class.php");

//have to do this ..

define("HOST", "localhost", TRUE);
define("PORT", 3306, TRUE);
define("USER", "root", TRUE);
define("PASS", "", TRUE);
define("DB", "test", TRUE);

/* class dabase {

  function connect() {
  $conn = mysql_connect(HOST, USER, PASS);
  mysql_select_db(DB);
  return $conn;
  }

  //    function count() {
  //        $this->connect();
  //        $query = mysql_query("select terminalID as nilai from transaction limit 25") or die(mysql_error());
  //        while ($fetch = mysql_fetch_assoc($query)) {
  //            $result[] = $fetch["nilai"];
  //        }
  //        $count = count($result);
  //        return $count;
  //    }

  function getTime() {
  $this->connect();
  $query = mysql_query("select jam as nilai from transaction") or die(mysql_error());
  while ($fetch = mysql_fetch_assoc($query)) {
  $result[] = $fetch["nilai"];
  }
  return $result;
  }

  function getData() {

  $time = $this->getTime();
  //$count = $this->count();
  $count = count($time);
  for ($i = 0; $i <= $count; $i++) {
  $old = strtotime($time[$i]);
  $now = strtotime('now');
  $interval = round(abs($now - $old) / 60);
  if ($interval >= 60) {
  $moOne +=1;
  } else {
  $leOne +=1;
  }
  }
  }

  }
 * 
 */

/* Create and populate the pData object */

//    $dabase = new Dabase();
$test = new Dabase_baru();
//    $dabase->getData();
$test->getSelisih();
$leOne = $test->leOne;
$moOne = $test->moOne;


//$data = $daBase->getData();
/* for ($i = 0; $i <= $count; $i++) {
  $old = strtotime($time[$i]);
  $now = strtotime('now');
  $interval = round(abs($now - $old) / 60);
  if ($interval >= 60) {
  $moOne +=1;
  } else {
  $leOne +=1;
  }
  }
 */
$MyData = new pData();
$MyData->addPoints(array($leOne, $moOne, 123), "ScoreA");
$MyData->setSerieDescription("ScoreA", "Application A");

/* Define the absissa serie */
$MyData->addPoints(array("<1 jam", ">1 jam", "Active TRX"), "Labels");
$MyData->setAbscissa("Labels");

/* Create the pChart object */
$myPicture = new pImage(340, 180, $MyData, TRUE);

/* Set the default font properties */
$myPicture->setFontProperties(array("FontName" => "lib/fonts/Forgotte.ttf", "FontSize" => 10, "R" => 80, "G" => 80, "B" => 80));

/* Create the pPie object */
$PieChart = new pPie($myPicture, $MyData);

/* Enable shadow computing */
$myPicture->setShadow(TRUE, array("X" => 3, "Y" => 3, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));

/* Draw a splitted pie chart */
$PieChart->draw3DPie(130, 90, array("Radius" => 100, "DataGapAngle" => 12, "DataGapRadius" => 10, "Border" => TRUE));

/* Write the legend box */
$myPicture->setFontProperties(array("FontName" => "lib/fonts/Silkscreen.ttf", "FontSize" => 6, "R" => 0, "G" => 0, "B" => 0));
$PieChart->drawPieLegend(140, 160, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL));

/* Render the picture (choose the best way) */
$myPicture->autoOutput("pictures/example.draw3DPie.transparent.png");
?>