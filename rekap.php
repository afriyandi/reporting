<?php
// copy justified APhe 2012
?>

<?php
include_once 'includes/includes.php';

if (!$blur->login_session) {
    header("Location: index.php");
    session_destroy();
}

include 'header.php';

//define('RESTRICTED', TRUE);
?>
<div id="TabbedPanels1" class="TabbedPanels">
    <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0" id="sum">Summary</li>
        <li class="TabbedPanelsTab" tabindex="0" id="stid">Search Active Transaction by TID</li>
        <li class="TabbedPanelsTab" tabindex="0" id="sdate">Search by Date</li>
        <li class="TabbedPanelsTab" tabindex="0" id="strans">Search by Transaction Type</li>        
    </ul>
    <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" id="ssumary">Coming soon ...</div>
        <div class="TabbedPanelsContent" id="sstid"></div>
        <div class="TabbedPanelsContent" id="ssdate"></div>
        <div class="TabbedPanelsContent" id="sstrans">Coming soon ...</div>
    </div>
</div>
<script type="text/javascript">
//    $("#sum").ready(function() {
//        $.blockUI({ 
//            message: "<img src = \"css/images/loading2.gif\" width=\"auto\">",
//            centerX: true,
//            centerY: true
//        });
//        $("#ssumary").load('chart.php', function(){                        
//            $.unblockUI();
//        })
//    })
    var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>


<?php
include 'footer.php';
?>