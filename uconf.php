<?php

// copy justified APhe 2012
?>
<?php

include_once 'includes/includes.php';

if (!$blur->login_session) {
    header("Location: index.php");
    session_destroy();
}

include 'header.php';
?>

<div id="TabbedPanels1" class="TabbedPanels">
    <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0" id="change_pass">Change Password</li>
        <li class="TabbedPanelsTab" tabindex="0" id="c_user">Create User</li>
        <li class="TabbedPanelsTab" tabindex="0" id="del_user">Delete User</li>
    </ul>
    <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" id="cpage"></div>
        <div class="TabbedPanelsContent" id="crepage"></div>
        <div class="TabbedPanelsContent" id="dpage"></div>
    </div>
</div>
<script type="text/javascript">
    var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
<script type="text/javascript">
$("#change_pass").ready(function(){$.blockUI({message:'<img src = "css/images/loading2.gif" width="auto">',centerX:!0,centerY:!0});$("#cpage").hide("fast");$("#cpage").load("umanage.php?method=change",function(){$.unblockUI();$("#cpage").show("slow")})});
</script> 
<?php
include 'footer.php';
?>