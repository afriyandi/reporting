<?php

// copy justified APhe 2012
?>

<?php

Class Param {

    private static $dabase = array(
        "HOST" => "localhost",
        "PORT" => "3306",
        "USER" => "root",
        "PASS" => "",
        "DB" => "mega",
        "COOKIES" => "monitoring",
        "SESSION" => "login",
        "PASS_LONG" => 8);

    public static function getDabase() {
        return self::$dabase;
    }

    private static $table = array("login_terminal" => "login_terminal",
        "change_tpin" => "change_tpin",
        "download_data" => "download_data",
        "info_tagihan" => "info_tagihan",
        "inquiry" => "inquiry",
        "monitoring" => "monitoring",
        "pelunasan" => "pelunasan",
        "pembayaran" => "payment",
        "settlement" => "settlement",
        "tlogout" => "tlogout",
        "ulogin" => "ulogin",
        "ulogout" => "ulogout",
        "stid" => "populate",
        "search_tid" => "search_tid",
        "search_date" => "search_date");

    public static function table() {
        return self::$table;
    }

}
?>