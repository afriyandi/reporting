<?php

// copy justified APhe 2012
?>

<?php

Class String {

    private static $logger = array("COOKIES" => "monitoring");

    public static function logger() {
        return self::$logger;
    }

    private static $login = array("Terminal ID",
        "Login Time"
    );
    private static $login_index = array("TERMINAL_ID",
        "TIME"
    );

    public static function login_index() {
        return self::$login_index;
    }

    public static function login() {
        return self::$login;
    }

    private static $ulogin = array("User ID",
        "Terminal Login",
        "Login Time"
    );
    private static $ulogin_index = array("TERMINAL_ID",
        "TERMINAL_ID",
        "TIME"
    );

    public static function Ulogin_index() {
        return self::$ulogin_index;
    }

    public static function Ulogin() {
        return self::$ulogin;
    }

    private static $change_tpin = array("Terminal ID",
        "User ID",
        "Change Time"
    );
    private static $change_tpin_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function change_tpin_index() {
        return self::$change_tpin_index;
    }

    public static function change_tpin() {
        return self::$change_tpin;
    }

    private static $download_data = array("Terminal ID",
        "User ID",
        "Download Time"
    );
    private static $download_data_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function download_data_index() {
        return self::$download_data_index;
    }

    public static function download_data() {
        return self::$download_data;
    }

    private static $info_tagihan = array("Terminal ID",
        "User ID",
        "Time"
    );
    private static $info_tagihan_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function info_tagihan_index() {
        return self::$info_tagihan_index;
    }

    public static function info_tagihan() {
        return self::$info_tagihan;
    }

    private static $inquiry = array("Terminal ID",
        "User ID",
        "Time"
    );
    private static $inquiry_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function inquiry_index() {
        return self::$inquiry_index;
    }

    public static function inquiry() {
        return self::$inquiry;
    }

    private static $monitoring = array("Terminal ID",
        "User ID",
        "Time"
    );
    private static $monitoring_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function monitoring_index() {
        return self::$monitoring_index;
    }

    public static function monitoring() {
        return self::$monitoring;
    }

    private static $pelunasan = array("Terminal ID",
        "User ID",
        "Time"
    );
    private static $pelunasan_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function pelunasan_index() {
        return self::$pelunasan_index;
    }

    public static function pelunasan() {
        return self::$pelunasan;
    }

    private static $pembayaran = array("Terminal ID",
        "User ID",
        "Time",
        "Payment Type",
        "Contract Number",
        "Customer Name",
        "Amount",
        "Instalment",
        "Due date"
    );
    private static $pembayaran_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME",
        "TTYPE",
        "NO_KONTRAK",
        "NAMA_NASABAH",
        "AMOUNT_PAYMENT",
        "ANGSURAN_KE",
        "JATUH_TEMPO"
    );

    public static function pembayaran_index() {
        return self::$pembayaran_index;
    }

    public static function pembayaran() {
        return self::$pembayaran;
    }

    private static $settlement = array("Terminal ID",
        "User ID",
        "Time"
    );
    private static $settlement_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME"
    );

    public static function settlement_index() {
        return self::$settlement_index;
    }

    public static function settlement() {
        return self::$settlement;
    }

    private static $search_tid = array("TERMINAL ID",
        "USER ID",
        "TIME",
        "DATE",
        "TTYPE",
        "CONTRACT NUM",
        "CUST. NAME",
        "AMOUNT",
        "INSTALLMENT",
        "DUE DATE",
        "BRANC ID",
        "REF NUMBER",
        "BATCH NUMBER",
        "TRX NUMBER"
    );
    private static $search_tid_index = array("TERMINAL_ID",
        "USER_ID",
        "TIME",
        "DATE",
        "TTYPE",
        "NO_KONTRAK",
        "NAMA_NASABAH",
        "AMOUNT_PAYMENT",
        "ANGSURAN_KE",
        "JATUH_TEMPO",
        "BRANCH_ID",
        "REF_NUMBER",
        "BATCH_NUMBER",
        "TRX_NUMBER"
    );

    public static function search_tid_index() {
        return self::$search_tid_index;
    }

    public static function search_tid() {
        return self::$search_tid;
    }
}

//define("COOKIES", "monitoring");
//define("FOXBASE", "includes/lib/foxbase");
//define("T_TID", "Terminal ID");
//define("T_USER", "User ID");
//define("T_TIME", "Waktu");
//define("T_TRANS", "Jenis Transaksi");
//define("T_AMOUNT", "Terbayar");
?>