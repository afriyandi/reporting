<?php

class Table {

    public $items;
    public $tags;
    public $desc;

    function __construct($params) {
        $this->items = array();
        $this->desc = array();
        $this->tags = $params;
    }

    function Col($params) {
        $this->items[] = new HTMLString("<td " . $params . " >");
        $this->desc[] = "td";
    }

    function Caption($params) {
        $this->items[] = new HTMLString("<caption>" . $params . "</caption>");
        $this->desc[] = "c";
    }

    function Header($params) {
        $this->items[] = new HTMLString("<th " . $params . ">");
        $this->desc[] = "th";
    }

    function Row($params) {
        $this->items[] = new HTMLString("<tr " . $params . " >");
        $this->desc[] = "tr";
    }

    function Add($params) {
        $this->items[] = new HTMLString($params);
        $this->desc[] = "s";
    }

    function Output() {
        $rows = 0;
        $cols = 0;
        $header = 0;

        echo "<table " . $this->tags . ">\n";

        for ($i = 0; $i < sizeof($this->desc); $i++) {
            if ($this->desc[$i] == "c")

            //outputting a header
                if ($this->desc[$i] == "th") {
                    if ($header)
                        echo "</th>\n";
                    $header++;
                }

            // outputting a row...
            if ($this->desc[$i] == "tr") {
                if ($cols)
                    echo "</td>\n";
                if ($rows)
                    echo "</tr>\n";
                $rows++;
            }

            // outputting a col
            else if ($this->desc[$i] == "td") {
                if ($cols)
                    echo "</td>\n";
                $cols++;
            }
            echo $this->items[$i]->Output();
        }

        echo "</td></tr></table> \n";
    }

    function size() {
        $count = 1;

        for ($i = 0; $i < sizeof($this->desc); $i++)
            if ($this->desc[$i] == "tr")
                $count++;

        return $count;
    }

}

class HTMLString {

    var $string;

    function __construct($params) {
        $this->string = $params;
    }

    function Output() {
        echo $this->string;
    }

}

class HTMLInput {

    public $Name;
    public $Maxlen;
    public $Value;
    public $Size;
    public $Other;
    public $id;

    function __construct($tName, $tMaxLen, $tValue, $tSize = -1, $method = "", $id) {
        $this->Name = $tName;
        $this->MaxLen = $tMaxLen;
        $this->Value = $tValue;
        if ($tSize == -1)
            $this->Size = $tMaxLen;
        else
            $this->Size = $tSize;
        $this->other = $other;
        $this->id = $id;
        ;
    }

    function Output() {
        unset($string);
        $string = "<input type=\"text\" id=\"" . $this->id . "\" value=\"" . $this->Value . "\" name=" . $this->Name;
        echo $string;

        if ($this->Size)
            echo " size=" . ($this->Size + 1);

        echo " " . $this->other;
//        echo " class=textbox ";
        if ($this->MaxLen)
            echo " maxlength=" . $this->MaxLen;
        echo "> \n";
    }

}

?>