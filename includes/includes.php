<?php

// copy justified APhe 2012
?>

<?php
error_reporting(E_ERROR);
session_start();

class Includes {

    private $class;

    public function __construct($class = array()) {
        $this->class = $class;
        spl_autoload_register(array($this, 'loader'));
    }

    function loader() {
        foreach ($this->class as $class)
            include $class . '.php';
    }

}
//loader standart class
new Includes(array("model", "user", "lib/strings", "lib/param", "morep", "method", "lib/html", "lib/Dabase"));
$get = new Get();
$get->connect();
$blur = new User();
$report = new Report();

$blur->loged();

date_default_timezone_set('Asia/Jakarta');

?>