<?php
// copy justified APhe 2012
?>
<style>
    table tbody tr#pink {
        background-image: url("css/ui-lightness/images/ui-bg_highlight-soft_100_eeeeee_1x100.png");
        background-position: center;
        background-repeat: repeat;
        font: 12px/14px sans-serif
    }
    table tbody tr#green {
        background-image: url("css/ui-lightness/images/ui-bg_glass_75_d0e5f5_1x400.png");
        background-position: center;
        background-repeat: repeat;
        font: 12px/14px sans-serif 
    }

    table tbody tr#main {
        background-image: url("css/ui-lightness/images/ui-bg_highlight-soft_75_ffe45c_1x100.png");
        background-position: center;
        background-repeat: repeat;
        cursor: default !important;
        font: 12px/14px sans-serif 

    }.main tr #td_main h2 {
        font-family: "Palatino Linotype", "Book Antiqua", "Palatino", "serif";
        font: 12px/14px sans-serif 
    }

    table tbody tr.data:hover{
        background: #98f07d !important;
        cursor: default !important;
    }

    table tbody tr.data.active{
        background: #988888 !important;
        cursor: default !important;
    }

</style>
<?php
include 'includes/includes.php';

$logged = $blur->loged();

if ($logged == false) {
    header("Location: index.php");
    session_destroy();
}

$method = $_GET["method"];
if (!empty($_GET["search"])) {
    $search = $_GET["search"];
}
if (!empty($_POST["query"])) {
    $query = json_encode(array("query" => $_POST["query"]));
} else {
    $tid = $_POST["tid"];
    $datum = $_POST["datum"];
    $query = json_encode(array("tid" => $tid, "datum" => $datum));
}
$string = new method($method, $search);
?>

<?php
$width = round(100 / $string->sum);
$table = new Table("width = \"100%\" border = \"1\"");
$table->Caption("Report For " . $string->header . " (" . date("d - M - Y") . ")");
$table->Row("id = \"main\"");
for ($i = 0; $i < $string->sum; $i++) {
    if ($i == 0)
        $table->Header("width=\"" . $width . "%\" height=\"20\"");
    else
        $table->Header("width=\"" . $width . "%\"");
    $table->Add($string->string[$i]);
}
$report->getTPS($string->table, $blur->comp, $string->search, $query);
$to_excel = serialize($report->tps);

for ($i = 0; $i < $report->count; $i++) {
    if ($i % 2 == 0) {
        $style = "pink";
    } else {
        $style = "green";
    }
    $index = $report->tps[$i];
    $table->Row("class =\"data\" id=\"" . $style . "\"");
    foreach ($string->index as $dim) {
        $table->Col("width=\"" . $width . "%\"");
        $table->Add($index[$dim]);
    }
}
$table->Output();
?>
</table>
